$(document).ready(function () {
      $('#nav li').hover(
        function () {
            
            $('ul', this).stop(true, true).delay(10).slideDown(60);
 
        }, 
        function () {
           
            $('ul', this).stop(true, true).slideUp(200);        
        }
    );
});